
public class ForAndWhile {

	public static void main(String[] args) {
		int paevadeArv = 0;
		boolean isTujuHea = true;
		for (; paevadeArv < 10 && isTujuHea; paevadeArv++) {
			System.out.println((paevadeArv + 1) + ". päev - lumi sajab");
			if (paevadeArv < 3) {
				System.out.println("lumememme ja ingleid saab teha, trenni ka");
			} else if (paevadeArv < 6) {
				System.out.println("Autol võiks ise puhastumise süsteem olla");
			} else {
				System.out.println("No aitab küll juba");
				isTujuHea = false;
			}
		}
		System.out.println((paevadeArv + 1) + ". päev Huh, sai läbi, kevad ei ole enam kaugel");

		System.out.println("----------------while--------------");
		int paevadeArvWhile = 0;
		boolean isTujuHeaWhile = true;
		while (paevadeArvWhile < 10 && isTujuHeaWhile) {
			System.out.println((paevadeArvWhile + 1) + ". päev - lumi sajab");
			if (paevadeArvWhile < 3) {
				System.out.println("lumememme ja ingleid saab teha, trenni ka");
			} else if (paevadeArvWhile < 6) {
				System.out.println("Autol võiks ise puhastumise süsteem olla");
			} else {
				System.out.println("No aitab küll juba");
				isTujuHeaWhile = false;
			}
			paevadeArvWhile++;
		}
		System.out.println((paevadeArvWhile + 1) + ". päev Huh, sai läbi, kevad ei ole enam kaugel");

		int i = 8;
		while (i < 8) {
			System.out.println("While ütleb, et i = " + i);
			i++;
		}

		int j = 8;
		do {
			System.out.println("Do-while ütleb, et j = " + j);
			j++;
		} while (j < 8);

	}

}
