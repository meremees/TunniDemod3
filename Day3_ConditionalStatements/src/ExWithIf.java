
public class ExWithIf {

	public static void main(String[] args) {
		int arv = Integer.parseInt(args[0]);
		if (arv % 2 == 0) {
			System.out.println("Arv " + arv + " on paaris");
		} else {
			System.out.println("Arv " + arv + " on paaritu");
		}

		System.out.println((arv % 2 == 0) ? "Arv " + arv + " on paaris" : "Arv " + arv + " on paaritu");
	}
}
