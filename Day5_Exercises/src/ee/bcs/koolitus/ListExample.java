package ee.bcs.koolitus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListExample {
	public static void main(String[] args) {
		List<List<String>> countriesWitCapitalNames = new ArrayList<>();
		countriesWitCapitalNames
				.addAll(Arrays.asList(new ArrayList<>(Arrays.asList("Helsinki", "Helsingi", "Helsinki", "Soome")),
						new ArrayList<>(Arrays.asList("Tallinn", "Tallinn", "Tallinn", "Eesti")),
						new ArrayList<>(Arrays.asList("Stockholm", "Stockholm", "Stockholm", "Rootsi")),
						new ArrayList<>(Arrays.asList("Copenhagen", "Kopenhaagen", "København", "Taani")),
						new ArrayList<>(Arrays.asList("Washington, D.C.", "Washington", " Washington, D.C.", "US"))));

		for (List<String> countryCapitalnames : countriesWitCapitalNames) {
			System.out.println(countryCapitalnames.get(1));
		}
	}

}
