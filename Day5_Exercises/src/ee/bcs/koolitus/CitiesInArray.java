package ee.bcs.koolitus;

import java.util.Arrays;

public class CitiesInArray {

	public static void main(String[] args) {
		String[][] countriesWithCapitalNames = new String[195][4];
		String[] finland = { "Helsinki", "Helsingi", "Helsinki", "Soome" };
		String[] estonia = { "Tallinn", "Tallinn", "Tallinn", "Eesti" };
		String[] sweden = { "Stockholm", "Stockholm", "Stockholm", "Rootsi" };
		String[] denmark = { "Copenhagen", "Kopenhaagen", "København", "Taani" };
		String[] us = { "Washington, D.C.", "Washington", " Washington, D.C.", "US" };
		countriesWithCapitalNames[0] = finland;
		countriesWithCapitalNames[1] = estonia;
		countriesWithCapitalNames[2] = sweden;
		countriesWithCapitalNames[3] = denmark;
		countriesWithCapitalNames[4] = us;

		// Trüki välja ainult eesti keelsed nimed
		for (String[] countryWithCapitalNames : countriesWithCapitalNames) {
			if (countryWithCapitalNames[0] != null) {
				System.out.println(countryWithCapitalNames[1]);
			}
		}
		System.out.println("---------------------------------------");
		// Lause koostamine
		for (String[] countryWithCapitalNames : countriesWithCapitalNames) {
			if (countryWithCapitalNames[0] != null) {
				System.out.println("Riik - " + countryWithCapitalNames[3] + ": pealinn - " + countryWithCapitalNames[1]
						+ "; inglise keeles - " + countryWithCapitalNames[0] + ", kohalikus keeles - "
						+ countryWithCapitalNames[2]);
			}
		}
		System.out.println("---------------------------------------");
		// ühel linnal võib kohalikus keeles mitu nime olla, variant 1 - Muudan linna
		// nime tekstilist sisu nii, et linnade nimesid eraldab semikoolon
		System.out.println("----ver 1 -----");
		countriesWithCapitalNames[0][2] = "Helsinki;Helsingfors";
		// Lause koostamine
		for (String[] countryWithCapitalNames : countriesWithCapitalNames) {
			if (countryWithCapitalNames[0] != null) {
				System.out.println("Riik - " + countryWithCapitalNames[3] + ": pealinn - " + countryWithCapitalNames[1]
						+ "; inglise keeles - " + countryWithCapitalNames[0] + ", kohalikus keeles - "
						+ countryWithCapitalNames[2]);
			}
		}

		System.out.println("---------------------------------------");
		// ühel linnal võib kohalikus keeles mitu nime olla, variant 2 - muudan
		// massiivi, viin riigi nime esimeseks ning lisan massivi lõppu vajaliku arvu
		// veerge, esialgu 1
		System.out.println("----ver 2 -----");
		countriesWithCapitalNames[0][2] = "Helsinki";
		for (int i = 0; i < countriesWithCapitalNames.length; i++) {
			// ajutine rea muutuja, mida kasutan veergude ümber tõstmiseks, teen selle kohe
			// ühe võrra pikema, kui varasem oli
			String[] tempCountryRow = new String[countriesWithCapitalNames[i].length + 1];
			// viimane veerg esimeseks
			tempCountryRow[0] = countriesWithCapitalNames[i][3];
			// ülejäänud veerud nihkega 1 samm edasi
			for (int columnCount = 0; columnCount < countriesWithCapitalNames[i].length - 1; columnCount++) {
				tempCountryRow[columnCount + 1] = countriesWithCapitalNames[i][columnCount];
			}
			// vahetan rea tableis uue ajutise rea vastu
			countriesWithCapitalNames[i] = tempCountryRow;
		}
		// lisan Helsingi teise kohaliku nime
		countriesWithCapitalNames[0][4] = "Helsingfors";
		// Lause koostamine
		for (String[] countryWithCapitalNames : countriesWithCapitalNames) {
			if (countryWithCapitalNames[0] != null) {
				String capitalNameInLocalLanguages = countryWithCapitalNames[3];
				if (countryWithCapitalNames[4] != null) {
					capitalNameInLocalLanguages = capitalNameInLocalLanguages + ", " + countryWithCapitalNames[4];
				}
				System.out.println("Riik - " + countryWithCapitalNames[0] + ": pealinn - " + countryWithCapitalNames[2]
						+ "; inglise keeles - " + countryWithCapitalNames[1] + ", kohalikus keeles - "
						+ capitalNameInLocalLanguages);
			}
		}

		System.out.println("---------------------------------------");
		// masiivis riiginimi country:riigi nimi
		for (String[] countryWithNames : countriesWithCapitalNames) {
			if (countryWithNames[0] != null) {
				countryWithNames[0] = "country:" + countryWithNames[0];
			}
		}
		// Lause koostamine
		for (String[] countryWithCapitalNames : countriesWithCapitalNames) {
			if (countryWithCapitalNames[0] != null) {
				String countryName = countryWithCapitalNames[0].split(":")[1];
				String capitalNameInLocalLanguages = countryWithCapitalNames[3];
				if (countryWithCapitalNames[4] != null) {
					capitalNameInLocalLanguages = capitalNameInLocalLanguages + ", " + countryWithCapitalNames[4];
				}
				System.out.println(
						"Riik - " + countryName + ": pealinn - " + countryWithCapitalNames[2] + "; inglise keeles - "
								+ countryWithCapitalNames[1] + ", kohalikus keeles - " + capitalNameInLocalLanguages);
			}
		}
		System.out.println("---------------------------------------");
		// Kurrunurru lisamine kolmandale, ilma üle kirjutamiseta
		// esmalt liigutan read 3+ (indeks = 2) edasi ühe rea võrra, eeldusel et nad ei
		// ole tühjad
		for (int i = countriesWithCapitalNames.length - 1; i >= 2; i--) {
			if (countriesWithCapitalNames[i][0] != null) {
				countriesWithCapitalNames[i + 1] = countriesWithCapitalNames[i];
			}
		}
		// Loon kurrunurru ja lisan kolmandale kohale
		String[] kurrunurru = { "Kurrunurruvutisaare Kuningriik", "Longstocking City", "Pikksuka linn", "Langstrump" };
		countriesWithCapitalNames[2] = kurrunurru;
		System.out.println(Arrays.deepToString(countriesWithCapitalNames));

		System.out.println("---------------------------------------");
		// Kurrunurru kustutamine
		for (int i = 3; i < countriesWithCapitalNames.length; i++) {
			if (countriesWithCapitalNames[i][0] != null) {
				countriesWithCapitalNames[i - 1] = countriesWithCapitalNames[i];
				if (countriesWithCapitalNames[i + 1][0] == null) {
					countriesWithCapitalNames[i] = countriesWithCapitalNames[i + 1];
					break;
				}
			}
		}
		System.out.println(Arrays.deepToString(countriesWithCapitalNames));
	}

}
